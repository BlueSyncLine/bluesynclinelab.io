<!DOCTYPE html>
<html lang="en">
    <head>
        <title>Fax demodulation experiments - BlueSyncLine</title>

        <meta charset="utf-8" />
        <link rel="stylesheet" type="text/css" href="../style.css" />
    </head>

    <body>
        <h1>Fax demodulation experiments</h1>
        <ul>
            <li><a href="#introduction">Introduction</a></li>
            <li><a href="#t30">T.30 procedures</a></li>
            <li><a href="#v17">V.17 demodulation</a></li>
            <li><a href="#source-code">Source code</a></li>
        </ul>

        <h2><a name="introduction">Introduction</a></h2>

        <figure>
            <img src="images/qam128.png" alt="A 128-QAM constellation diagram; 128 points in a cross-shaped pattern." />
            <figcaption>A 128-QAM (V.17 at 14.4kbit/s) constellation diagram.</figcaption>
        </figure>

        <p>
            Inspired by Gough Lui's pages (<a href="https://goughlui.com/2013/02/13/sounds-of-fax-modes-and-ecm/">1</a>, <a href="https://goughlui.com/project-fax/fax-technicalities-audio-samples/">2</a>) with audio of T.30 (phone line) fax transmissions, I decided that trying to demodulate the original data back from the audio would be a fun DSP challenge.
        </p>

        <figure>
            <img class="photo" src="images/fax-call.png" alt="A spectrogram of a fax call with sections of HDLC negotiations and fax data prominent." />
            <figcaption>One of the recordings viewed in Audacity.</figcaption>
        </figure>

        <h2><a name="t30">T.30 procedures</a></h2>
        <ul>
            <li><a href="#t30-intro">Introduction</a></li>
            <li><a href="#t30-v21">V.21</a></li>
        </ul>

        <figure>
            <img class="photo" src="images/t30-cover.png" alt="The cover sheet of the ITU-T T.30 standard" />
            <figcaption>The T.30 standard is available on ITU's website <a href="https://www.itu.int/ITU-T/recommendations/rec.aspx?rec=6478&lang=en">here</a>.</figcaption>
        </figure>

        <h3><a name="t30-intro">Introduction</a></h3>
        <p>
            The first thing I needed to implement was demodulation of T.30 signalling which is used to establish a fax connection.
        </p>

        <p>
            The specification used for this is <a href="https://www.itu.int/ITU-T/recommendations/rec.aspx?rec=2731&lang=en">V.21</a>, frequency-shift keying at 300 bits per second.
            Unlike the regular asynchronous approach, in this instance the mode is <b>synchronous</b>, i.e. no start/stop bits are used, so the clock recovery can only be performed based on transitions in the data stream.
        </p>

        <p>
            The packets used by the T.30 protocol are encapsulated using the synchronous mode of <a href="https://en.wikipedia.org/wiki/High-Level_Data_Link_Control">HDLC</a>.
        </p>

        <h3><a name="t30-v21">V.21</a></h3>
        <h4><a name="t30-v21-demod">Demodulation</a></h4>
        <figure>
            <img src="images/v21.png" alt="A plot with two traces, V.21 data and the phase of the recovered clock signal." />
            <figcaption>V.21 demodulation; the orange trace is the clock phase.</figcaption>
        </figure>

        <p>
            To obtain the bits, I first mix down the carrier (by multiplying the incoming samples with a complex wave at the negative carrier frequency), pass the resulting baseband signal through a <a href="#v17-filtering-wsinc">FIR filter</a>, and then compute the angle of every outgoing sample multiplied by the conjugate of the previous one.
            <br />
            This results in a varying level (plotted above as a blue trace; it's above zero if the input frequency is higher than the carrier, below if lower, zero if equal) that can then be sliced to determine the bit values (incidentally, V.21 has the zero bit on a higher frequency).
        </p>

        <h4><a name="t30-v21-clock">Clock</a></h4>
        <p>
            I sample the bits by having a variable for the current position into the bit period, incrementing it every sample by the ratio of the baud rate to the sampling rate, and reading the current bit when this variable overflows past 1 (applying <code>fmod</code> to roll it over).
        </p>

        <h4><a name="t30-v21-clockrec">Clock recovery</a></h4>
        <p>
            The clock recovery works by nudging this variable towards 0.5 whenever a zero crossing occurs, thus keeping the transition right in the middle of the bit period, making it an optimal choice to sample the signal at the point at which the clock variable reaches or exceeds 1.0 and is folded back.
        </p>

        <h2><a name="v17">V.17 demodulation</a></h2>
        <ul>
            <li><a href="#v17-intro">Introduction</a></li>
            <li><a href="#v17-sampling">Sampling and clock recovery</a></li>
            <li><a href="#v17-constellation">Constellation demapping</a></li>
            <li><a href="#v17-trellis">Trellis decoder</a></li>
        </ul>

        <h3><a name="v17-intro">Introduction</a></h3>
        <figure>
            <img class="photo" src="images/v17-cover.png" alt="The cover sheet of the ITU-T V.17 fax standard" />
            <figcaption>The V.17 standard, available <a href="https://www.itu.int/ITU-T/recommendations/rec.aspx?rec=2726&lang=en">here</a> (<a href="https://www.itu.int/ITU-T/recommendations/rec.aspx?rec=4776&lang=en">errata</a>).</figcaption>
        </figure>

        <figure>
            <a href="diagram/v17.png" target="_blank"><img class="photo" src="diagram/v17.png" alt="A block diagram of the demodulator" /></a>
            <figcaption>A block diagram of the implementation (click to open the full image)</figcaption>
        </figure>

        <p>
            After implementing a means to decode and (primitively) parse the T.30 messages, I focused on demodulating the signal which carries the actual fax data, aiming at the V.17 files I found due to the fact that this mode at 14400 bits per second was featured the most in Gough's recordings (probably due to being the first option negotiated by many faxes and modems) and also as a result of relative complexity of the standard compared to the others (V.29 and V.27ter), as it involves <a href="#v17-trellis">trellis modulation</a> and very fragile 64- and 128-QAM constellations, which posed nice challenges.
        </p>

        <h3><a name="v17-sampling">Sampling and clock recovery</a></h3>
        <h4><a name="v17-sampling-resamp">Resampler</a></h4>
        <p>
            For two reasons, I had to first implement a resampler.
        </p>

        <ul>
            <li>The sample rate of the files that I'm working with is 8 kHz, which isn't divisible by the 2400 Hz baud rate.</li>
            <li>Lots of oversampling are required anyway, as the QAM signal is very sensitive to the sampling phase.</li>
        </ul>

        <figure>
            <video src="images/qam-phase.mp4" controls="controls"></video>
            <figcaption>Constellation diagram versus the sampling point, QAM-128, 80 samples per symbol.</figcaption>
        </figure>

        <p>
            The structure worked by having a <a href="#v17-filtering">low-pass filter</a> that, for every incoming sample, was fed with that sample followed by N-1 zeroes, with N being the interpolation factor.
            Due to the <a href="#v17-adapteq-working-resamp">adaptive equalization approach</a> I went with, I had to change those zeroes to repeats of the same sample.
        </p>

        <p>
            The current adaptive equalization approach also alters the filter coefficients after the original root-raised cosine response is computed since the same filter is used for multiple purposes.
        </p>

        <p>
            For efficiency, a multiply-accumulate calculation of the resampling filter is only carried out when an output sample is requested by the clocking code, since simply updating the filter's delay line doesn't require such a computation.
        </p>
        <h4><a name="v17-sampling-clock">Clocking</a></h4>
        <p>
            After the resampler, the symbols are chosen from the samples by a structure similar to <a href="#t30-v21-clock">the one used in the V.21 demodulator</a>.
        </p>

        <h4><a name="v17-sampling-clockrec">Clock recovery</a></h4>
        <p>
            The Gardner clock recovery algorithm is employed to correct the sampling phase. Since two samples per symbol are required, the output of the resampler is at 4800 Hz, double of the 2400 Hz baud rate.
        </p>

        <h3><a name="v17-agc">Automatic gain control</a></h3>
        <p>
            A single-pole IIR filter holds the current amplitude and is corrected based on the corresponding error value from the constellation decisions.
        </p>

        <h3><a name="v17-constellation">Constellation demapping</a></h3>
        <p>
            For converting the symbol's complex coordinates back into a data point, I initially attempted using a grid that the integer-rounded coordinates were used to index.
        </p>

        <p>
            I eventually switched to an approach of simply iterating the coordinates of every point, comparing, and using the closest one as the symbol decision, which was easier to implement and felt as if it was more accurate than the rounded coordinates (although this requires more computation).
        </p>

        <h3><a name="v17-filtering">Filtering</a></h3>
        <h4><a name="v17-filtering-wsinc">Windowed-sinc FIR</a></h4>
        <p>
            The initial approach to filtering the mixed-down carrier was using a Blackman-windowed sinc function, just as in the V.21 decoder described above.
        </p>
        <p>
            I found out that despite the carrier only occupying &plusmn;1200 Hz, 1600 worked much better as the filter bandwidth and seemed to be the exact value required after some experimentation.
        </p>

        <figure>
            <img src="images/v17-training-bw-narrow.png" alt="A QPSK constellation diagram with the dots expanded due to distortion." />
            <figcaption>Training symbols, the filter bandwidth is too narrow.</figcaption>
        </figure>

        <figure>
            <img src="images/v17-training-bw-correct.png" alt="A QPSK constellation diagram with very small dots." />
            <figcaption>Filter bandwidth which seemed correct.</figcaption>
        </figure>

        <p>
            Going beyond 1600 Hz started introducing distortion and noise again.
        </p>

        <h4><a name="v17-filtering-rrc">Root-raised cosine filter</a></h4>
        <p>
            I started wondering whether a root-raised cosine filter for the carrier would work better than a simple windowed-sinc and decided to implement one; 0.35 appears to be the best beta coefficient for this.
        </p>

        <h3><a name="v17-adapteq">Adaptive equalizer</a></h3>
        <h4><a name="v17-adapteq-fail">Failed approach</a></h4>
        <p>
            My first approach to implementing the adaptive equalizer was using the <a href="https://en.wikipedia.org/wiki/Least_mean_squares_filter#Normalized_least_mean_squares_filter_(NLMS)">NLMS</a> algorithm.
        </p>

        <p>
            I tried using the filter after the resampler, on the outgoing symbols. This didn't seem to have much effect on the SNR...
        </p>

        <p>
            There are several possible reasons for the failure of this approach, among them:
        </p>

        <ul>
            <li><del><b>Using only a single sample per symbol</b></del> - my original motivation for replacing the equalizer, however an experiment done after this article was written demonstrated that it'd still work correctly.</li>
            <li><b>Interference with clock recovery</b> - as the equalizer taps were updated, the Gardner algorithm seemed to have troubles tracking the signal.</li>
            <li><b>Only applying the equalizer to delayed samples</b> - the equalizer's filter should have been centered around the sampling point as opposed from starting there, since a "negative delay" might be possible if the "best" sampling point is after the first sample affected by a new symbol.</li>
        </ul>

        <p>
            <b>Thus, </b> the approach described above had been abandoned in favor of a <a href="#v17-adapteq-working">different one</a>.
        </p>

        <h4><a name="v17-adapteq-bad-recordings">"Bad" recordings</a></h4>
        <figure>
            <img src="images/qam128-bad.png" alt="A distorted constellation diagram with all points in a single clump." />
            <figcaption>A distorted 128-QAM constellation diagram.</figcaption>
        </figure>

        <p>
            I discovered that some signal recordings produced a nice constellation diagram, yet some only resulted in a jumbled mess.
        </p>

        <p>
            After lots of experimentation, tweaking the clock recovery coefficient, resampling rate and such, I decided that a different equalization strategy was needed.
        </p>

        <h4><a name="v17-adapteq-working">Working approach</a></h4>
        <p>
            I thought that the issue occured as a result of using the symbols directly instead of oversampling them (<b>I realized that wasn't the case later, </b> but other <a href="#v17-adapteq-fail">reasons</a> remain).
        </p>

        <p>
            Thus, I started using the NLMS algorithm to tweak the coefficients of the resampling filter itself, which now performs a triple duty:
        </p>

        <ul>
            <li><b>Resampling filter</b> - provides a low-pass frequency response which removes the aliasing from interpolation.</li>
            <li><b>Root-raised cosine filter</b> - matched filtering for the carrier.</li>
            <li><b>Adaptive equalizer</b> - raises the SNR by undoing the effects of the transmission channel and the signal capturing device.</li>
        </ul>

        <p>
            The root-raised cosine filter initially has real-value coefficients, but when modified by the equalization code they become complex.
        </p>

        <figure>
            <img src="images/equalizer-spectrum.png" alt="A comparison of the root-raised-cosine filter's frequency response with the computer equalizer, which looks more noisy" />
            <figcaption>The original root-raised cosine frequency response (blue) versus that of the equalizer filter (orange).</figcaption>
        </figure>

        <figure>
            <img src="images/equalizer-spectrum-closeup.png" alt="A close-up, showing the original filter to have a flat response while the equalizer's is asymmetrical." />
            <figcaption>A close-up of the figure above.</figcaption>
        </figure>

        <h4><a name="v17-adapteq-working-resamp">Resampler remark</a></h4>
        <p>
            The new equalization approach (i.e. using the resampling filter for equalization as well) required the zero samples inserted by the resampler for interpolation to be replaced with repeats, since otherwise the coefficients at certain indices weren't being calculated as a result of gaps in the input signal.
        </p>

        <p>
            Notably, interpolating the samples by repetition with just an RRC filter (i.e. without an equalizer) yielded a significantly lower SNR than inserting zeroes.
        </p>

        <h3><a name="v17-trellis">Trellis decoder</a></h3>
        <h4><a name="v17-trellis-description">Description</a></h4>
        <p>
            V.17 features a 8-state trellis encoder working on the bottom three bits of every symbol (with one of them being the output of the convolutional encoder).
        </p>

        <p>
            Eight paths are maintained, one for every possible state. Whenever two converge on a single state, the one with the least metric wins.
        </p>

        <h4><a name="v17-trellis-previous">Previous approach</a></h4>
        <p>
            <del>The errors are detected from a mismatch of the lowest bit of a symbol with the expected value, which causes the path to split into four (or less for points on the edges of the constellation), corresponding to the closest neighbors of a given symbol in the constellation.</del>
        </p>

        <h4><a name="v17-trellis-approach">Current approach</a></h4>
        <p>
            The current approach to trellis decoding is based on the concepts of <a href="https://www.ti.com/lit/an/spra099/spra099.pdf">Texas Instruments application note SPRA099</a>, with eight neighbors considered and the total squared distance being used as the path metric.
        </p>

        <p>
            This has been found to produce a lot of improvement in the error rate compared to my previous strategy.
        </p>

        <h4><a name="v17-trellis-constellation">Notable property of the V.17 signal constellations</a></h4>
        <p>
            The V.17 constellations are designed to be tolerant to 90-degree rotations, allowing for easy recovery after a loss of both clocking and the reference phase. <small>[doi:10.1109/jsac.1984.1146102]</small>
        </p>

        <h4><a name="v17-trellis-tblen">Traceback length</a></h4>
        <p>
            The decoder stores 32 previous symbols of every path as suggested by this <a href="https://en.wikipedia.org/wiki/Viterbi_decoder#Traceback">rule of thumb</a>.
        </p>

        <h2><a name="source-code">Source code</a></h2>
        <p>
            The source code for this project is available <a href="https://gitlab.com/BlueSyncLine/faxdec">here</a>.
        </p>

        <p>
            <a href="../index.html">Home</a>
        </p>
    </body>
</html>
