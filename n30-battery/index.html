<!DOCTYPE html>
<html lang="en">
    <head>
        <title>Acer N30 battery controller emulation</title>

        <meta charset="utf-8" />
        <link rel="stylesheet" type="text/css" href="../style.css" />
    </head>

    <body>
        <h1>Acer N30 battery controller emulation</h1>
        <ul>
            <li><a href="#introduction">Introduction</a></li>
            <li><a href="#cable">Cable and flashing</a></li>
            <li><a href="#power">Powering up</a></li>
            <li><a href="#emulation">Emulating the battery controller</a></li>
            <li><a href="#project">Project details</a></li>
        </ul>

        <h2><a name="introduction">Introduction</a></h2>
        <figure>
            <img src="images/n30.jpg" style="width:15%" alt="The Acer N30 PDA, displaying the battery percentage screen" />
            <figcaption>Acer N30</figcaption>
        </figure>

        <p>
            Recently, I obtained three Acer N30 PDAs from Germany.
            The first attempt to mail them failed due to the inclusion of lithium batteries, so the seller kindly offered to take them out and resend, to which I agreed.
        </p>

        <p>
            Little did I know that the batteries in this particular series contain an integrated controller...
        </p>

        <h2><a name="cable">Cable and flashing</a></h2>
        <p>
            Together with the PDAs, I ordered a pair of car charging adapters specifically for the proprietary 26-pin connector, since this seemed like the cheapest way to obtain it.
        </p>

        <p>
            Nicely, the connectors contained all the pins and not just the power ones, so I was also able to prepare a USB cable and flash an English-language firmware instead of the original German one.
        </p>

        <p>
            This very old flashing tool is suitable for the purpose and has been tested to run on a modern system: <a href="https://github.com/pinkavaj/romtools">https://github.com/pinkavaj/romtools</a>.
        </p>

        <p>
            The UBI image extractable from the Windows Mobile update package is usable with the "dnw" utility from there.
        </p>

        <p>
            The connector's pinout can be found here: <a href="https://allpinouts.org/pinouts/connectors/pda/acer-n30-n35-n310-n311-n50">https://allpinouts.org/pinouts/connectors/pda/acer-n30-n35-n310-n311-n50</a>.
        </p>

        <p>
            I attached a regular USB plug to the charging power, ground, and client D+/D- pins.
        </p>

        <p>
            Note:
        </p>
        <ul>
            <li>The data lines appear to be swapped - pin 13 is actually D+, not D-.</li>
            <li>Pin 9 needs to be hooked to VCC, otherwise the PDA will not respond to the connection.</li>
        </ul>

        <p>
            Inside of the particular connectors that I received was a weird arrangement with four overlapping rows of solder terminals, so double-checking and very careful inspection of where each pin actually goes has been essential.
        </p>

        <h2><a name="power">Powering up</a></h2>
        <p>
            Even without a battery, the PDA starts up as soon as power is applied to the connector.
            However, the troubles began while I was attempting to apply a typical Li-Ion voltage to the battery terminals inside of the device in preparation for sticking a real battery there.
        </p>

        <p>
            It booted, sure, but immediately reported that the main battery was supposedly very low and kept going to the sleep mode in an effort to save power (on WinCE devices of the era, the user data and configuration is stored in a RAM disk and is lost if the battery is fully discharged).
        </p>

        <p>
            After some searching, I realized that this issue has been encountered by other owners attempting to replace the original battery, and the proposed solution was just to remove the controller board from a donor and attach a new cell to it.
        </p>

        <p>
            I didn't give up and sought the service manual (for which I ended up paying a symbolic amount). It did contain a useful description of the system, yet the most important part of the diagram - discussing the battery controller - was blurry.
        </p>

        <p>
            Inspecting the motherboard, I found a chip marked "AZX", which turned out to be the <a href="https://www.ti.com/product/BQ24025">bq24025</a> charger from Texas Instruments. The diagram listed a MSP430F1111 microcontroller communicating with something vaguely discernible as bq**00 and appearing related to the battery.
        </p>

        <figure>
            <img src="images/service-manual.png" alt="An excerpt of the service manual" />
            <figcaption>A part of the system diagram from the service manual, showing the battery controller.</figcaption>
        </figure>

        <p>
            I realized that said chip has no means of measuring the battery voltage, so something else had to be involved. I kept looking for other chips of the BQ series until I discovered the <a href="https://www.ti.com/product/BQ26500">bq26500</a>.
        </p>

        <p>
            It is a small "gas gauge" chip for Li-Ion batteries, communicating via the HDQ protocol.
        </p>

        <p>
            I was aware that the original battery uses a 4-pin connector, with three of those being power, ground, and the NTC thermistor connection for the charging chip, but there was a mysterious extra wire.
        </p>

        <p>
            Could that wire be used for this? Indeed, it was.
        </p>

        <p>
            After hooking up a logic analyzer to the extra wire, I immediately noticed constant activity.
        </p>

        <p>
            This prompted me to start the effort described henceforth.
        </p>

        <h2><a name="emulation">Emulating the battery controller</a></h2>
        <figure>
            <img src="images/ti-hdq.png" alt="An excerpt from the TI document on the HDQ protocol of their battery controller chips" />
            <figcaption>A basic description of the HDQ protocol from TI - <a href="https://www.ti.com/lit/an/slua408a/slua408a.pdf">SLUA408A</a></figcaption>
        </figure>

        <p>
            Guided by the <a href="https://www.ti.com/lit/ds/symlink/bq26500.pdf">datasheet</a> of the bq26500, I quickly prepared a STM32F401RE Nucleo board as the prototype.
        </p>

        <p>
            After powering up, the PDA constantly tries reading the register 0x7f, which can be used either as a configuration value or an arbitrary programmable ID byte.
        </p>

        <p>
            If a response to this is seen, several interesting things are performed:
        </p>

        <ul>
            <li>Register 0x7e with an identical role is also read. Returning a zero byte for both of them seemed to work initially, but see below.</li>
            <li>0x02 is written with some value and then read back.</li>
            <li>While a charging voltage is applied, 0x0a - the status flag register - is accessed.</li>
            <li>Most importantly, an access to 0x0b happens. This register is supposed to hold the battery percentage.</li>
        </ul>

        <figure>
            <img src="images/hdq-on-logic-analyzer.png" alt="A logic analyzer trace of the HDQ communications" />
            <figcaption>HDQ transfer: A read of register 0x0b, with the response 0x64 (100 decimal)</figcaption>
        </figure>

        <p>
            Setting a response to 0x0b reads, I was able to control the displayed battery percentage almost exactly, and the device no longer complained!
        </p>

        <p>
            It also turned out that the software runs the returned percentage value through some sort of a non-linear function or a lookup table, possibly for better precision.
        </p>

        <p>
            With that out of the way, the next question was charging, and the charger chip refused to produce an output voltage.
        </p>

        <p>
            It actively measures the resistance at the NTC terminal by applying a specific current and comparing the resulting voltage against two thresholds.
        </p>

        <p>
            Since this was not critical for charging and omitted by many devices nowadays, I simply soldered a 10k SMD resistor between the NTC pin and ground, keeping it satisfied.
        </p>

        <h2><a name="ch32v003">CH32V003 implementation</a></h2>
        <p>
            I had plenty of the <a href="https://www.wch-ic.com/products/CH32V003.html">CH32V003</a>, the famous 3-cent RISC-V microcontroller, and it seemed to be usable for the job.
        </p>

        <p>
            Importantly, it features a deep sleep ("standby") mode, which would prevent excessive battery drain when idle, and the datasheet promises a full wakeup in about ~250μS.
        </p>

        <p>
            This allows waking up on the start bit (the "Break signal" of the HDQ transmission) in time to receive the data word and respond.
        </p>

        <p>
            For the actual battery gauging, I chose to measure the voltage, which is less accurate than the original sophisticated coulomb counting approach yet still very much usable.
        </p>

        <p>
            The CH32V003 has an internal reference ADC channel fixed to 1.2V, so it is possible to measure Vcc (which is the full-scale voltage of the ADC) against that; I was inspired by a writeup that I saw much earlier: <a href="https://hackaday.com/2023/12/21/arduino-measures-remaining-battery-power-with-zero-components-no-i-o-pin">Hackaday</a>.
        </p>

        <p>
            I prepared another prototype (with the same functionality as the STM32 arrangement described above) on an evaluation board and finally flashed the MCU in a SOIC-8 package, soldering it down onto the motherboard and connecting its pins to the proper locations with enameled wire. A Nokia BL-5CT cell fit nicely into the case.
        </p>

        <p>
            It let me charge the battery once and discharged correctly, however charging again was completely futile.
        </p>

        <p>
            Now, upon applying external power, the charging got briefly activated and promptly shut down - what gives? I disassembled the PDA again.
        </p>

        <p>
            It turned out that the charging process is being disabled by the software (by driving the CE pin of the charger high) after a series of HDQ accesses - but why? What didn't the driver like?
        </p>

        <p>
            With trial and error, I realized that the zero values returned in 0x7e/0x7f were the culprit.
        </p>

        <p>
            Returning another byte (I tried 0x55 and other values) immediately changed the access pattern, and now the PDA wanted to see register 0x0a as well. Somehow, I've seen this happen before but didn't realize that specific conditions were required…
        </p>

        <figure>
            <img src="images/reg-0x0a.png" alt="A section of the bq26500 datasheet discussing register 0x0a" />
            <figcaption>Register 0x0a, from the <a href="https://www.ti.com/lit/ds/symlink/bq26500.pdf">bq26500 datasheet</a></figcaption>
        </figure>

        <p>
            Bit 7 of 0x0a is the charging status, so returning 0x80 there should suffice. This has finally let me charge the device. <b>Success!</b>
        </p>

        <h2><a name="project">Project details</a></h2>

        <p>
            The firmware, based on the <a href="https://github.com/cnlohr/ch32v003fun">ch32v003fun</a> library, can be found in <a href="https://gitlab.com/BlueSyncLine/n30-bq-emu">this repo</a>.
        </p>

        <h3>Limitations</h3>
        <p>
            While charging, the battery voltage is higher, making the indication inaccurate. This is where the original coulomb counter shines; however, the PDA appears to rely on the charging chip to determine when the cycle really stops, so that at least should be indicated properly.
        </p>

        <p>
            The voltage also fluctuates somewhat under load. There is no current measurement implemented to compensate for this, although this is possible in theory by using a shunt resistor in tandem with the MCU's built-in opamp and ADC.
        </p>

        <h3>Hardware</h3>
        <table style="border: none">
            <tr>
            <td style="border: none">
                <figure>
                    <img src="images/motherboard-connections.jpg" alt="A connection diagram for the N30's motherboard" />
                    <figcaption>The relevant pins on the motherboard of the PDA</figcaption>
                </figure>
            </td>
            <td style="border: none">
                <figure>
                    <img src="images/installed.jpg" alt="A CH32V003 microcontroller installed on the PDA's motherboard" />
                    <figcaption>CH32V003 mounted on the motherboard</figcaption>
                </figure>
            </td>
            </tr>
        </table>

        <p>
            I chose to locate the microcontroller directly on the motherboard of the PDA due to space constraints and not having the original proprietary thin 1.25mm battery connector. It was done rather crudely, but works. A decoupling capacitor across the power pins of the MCU certainly wouldn't hurt.
        </p>

        <p>
            <a href="../index.html">Home</a>
        </p>

    </body>
</html>
